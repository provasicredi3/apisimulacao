package br.com.sicredi.api.funcional;

import br.com.sicredi.api.factory.SimulacaoDataFactory;
import br.com.sicredi.api.pojo.Simulacao;
import org.apache.http.HttpStatus;
import org.junit.Test;

import java.io.IOException;

import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.*;

public class TesteDeletarSimulacaoDeCredito extends TesteBase {
    private static final String SIMULACAO_CREDITO_ENDPOINT = "/simulacoes/";
    @Test
    public void testeDeletarSimulacaoComSucesso() throws IOException {
        Simulacao simulacao = SimulacaoDataFactory.criarSimulacaoComSucesso();
        int capturaId;

        capturaId = given()
            .body(simulacao)
        .when()
            .post(SIMULACAO_CREDITO_ENDPOINT)
        .then().log().all()
            .extract()
                .path("id");


        given()
        .when()
            .delete(SIMULACAO_CREDITO_ENDPOINT+capturaId)
        .then()
            .assertThat()
                .statusCode(HttpStatus.SC_NO_CONTENT);

    }

    @Test
    public void testeDeletarSimulacaoInexistente() throws IOException {
        Simulacao simulacao = SimulacaoDataFactory.criarSimulacaoComSucesso();
        int capturaId;


        capturaId = given()
            .body(simulacao)
        .when()
            .post(SIMULACAO_CREDITO_ENDPOINT)
        .then()
            .extract()
                .path("id");

        int soma = capturaId + 1;
        given()
        .when()
            .delete(SIMULACAO_CREDITO_ENDPOINT+soma)
        .then()
            .assertThat()
                .statusCode(HttpStatus.SC_NOT_FOUND)
                .body(is("OK"));

    }
}
