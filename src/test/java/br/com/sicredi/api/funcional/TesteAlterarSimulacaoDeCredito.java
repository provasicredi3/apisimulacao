package br.com.sicredi.api.funcional;


import br.com.sicredi.api.factory.SimulacaoDataFactory;
import br.com.sicredi.api.pojo.Simulacao;
import org.apache.http.HttpStatus;
import org.junit.Test;
import java.io.IOException;
import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.*;

public class TesteAlterarSimulacaoDeCredito extends TesteBase {
    private static final String SIMULACAO_CREDITO_ENDPOINT = "/simulacoes/";
    @Test
    public void testeAlterarSimulacaoComSucesso() throws IOException {
        Simulacao simulacao = SimulacaoDataFactory.criarSimulacaoComSucesso();
        Simulacao simulacaoAlterada = SimulacaoDataFactory.criarSimulacaoComSucesso();
        String capturaCpf;

         capturaCpf = given()
            .body(simulacao)
        .when()
            .post(SIMULACAO_CREDITO_ENDPOINT)
        .then()
             .extract()
                .path("cpf");

       given()
            .body(simulacaoAlterada)
        .when()
            .put(SIMULACAO_CREDITO_ENDPOINT+capturaCpf)
        .then()
           .assertThat()
               .statusCode(HttpStatus.SC_CREATED)
               .body("id", notNullValue())
               .body("nome", equalTo(simulacaoAlterada.getNome()))
               .body("cpf", equalTo(simulacaoAlterada.getCpf()))
               .body("email", equalTo(simulacaoAlterada.getEmail()))
               .body("valor", equalTo(simulacaoAlterada.getValor()))
               .body("parcelas", equalTo(simulacaoAlterada.getParcelas()))
               .body("seguro", equalTo(simulacaoAlterada.isSeguro()));
    }

    @Test
    public void testeAlterarSimulacaoCpfNaoEncontrado() throws IOException {
        Simulacao simulacao = SimulacaoDataFactory.criarSimulacaoComSucesso();
        String cpf = simulacao.getCpf();

        given()
            .body(simulacao)
        .when()
            .put(SIMULACAO_CREDITO_ENDPOINT+cpf)
        .then()
            .assertThat()
                .statusCode(HttpStatus.SC_NOT_FOUND)
                .body("id", nullValue())
                .body("mensagem", equalToIgnoringCase("CPF "+cpf+" não encontrado"));
    }
}
