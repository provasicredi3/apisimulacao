package br.com.sicredi.api.funcional;

import br.com.sicredi.api.utils.GeradorDeDadosAleatorios;
import org.apache.http.HttpStatus;
import org.junit.Test;
import static io.restassured.RestAssured.*;
import static org.hamcrest.Matchers.equalToIgnoringCase;

public class TesteConsultarRestricaoDeCredito extends TesteBase {
    private static final String CONSULTA_CPF_RESTRITO_ENDPOINT = "/restricoes/";
    @Test
    public void testGetCpfComRestricao(){
        String cpf = GeradorDeDadosAleatorios.retornarCpfRestrito();

        given()
        .when()
            .get(CONSULTA_CPF_RESTRITO_ENDPOINT+cpf)
        .then()
            .assertThat()
                .statusCode(HttpStatus.SC_OK)
                .body("mensagem", equalToIgnoringCase("o cpf " + cpf + " tem problema"));
    }

    @Test
    public void testGetRestricaoComCpfInvalido(){
        String cpf = GeradorDeDadosAleatorios.retornarCpfInvalido();

        given()
        .when()
            .get(CONSULTA_CPF_RESTRITO_ENDPOINT+cpf)
        .then()
            .assertThat()
                .statusCode(HttpStatus.SC_NO_CONTENT);
    }

    @Test
    public void testGetCpfValidoSemRestricao(){
        String cpf = GeradorDeDadosAleatorios.gerarCpfValidoSemRestricao();

        given()
        .when()
            .get(CONSULTA_CPF_RESTRITO_ENDPOINT+cpf)
        .then()
            .assertThat()
                .statusCode(HttpStatus.SC_NO_CONTENT);
    }

}
