package br.com.sicredi.api.funcional;

import br.com.sicredi.api.factory.SimulacaoDataFactory;
import br.com.sicredi.api.pojo.Simulacao;
import org.apache.http.HttpStatus;
import org.junit.Test;
import java.io.IOException;
import static io.restassured.RestAssured.*;
import static org.hamcrest.Matchers.*;

public class TesteInserirSimulacaoDeCredito extends TesteBase {

    private static final String SIMULACAO_CREDITO_ENDPOINT = "/simulacoes/";

    @Test
    public void testeInserirSimulacaoComSucesso() throws IOException {
        Simulacao simulacao = SimulacaoDataFactory.criarSimulacaoComSucesso();

        given()
            .body(simulacao)
        .when()
            .post(SIMULACAO_CREDITO_ENDPOINT)
        .then()
            .assertThat()
                .statusCode(HttpStatus.SC_CREATED)
                .body("id", notNullValue())
                .body("nome", equalTo(simulacao.getNome()))
                .body("cpf", equalTo(simulacao.getCpf()))
                .body("email", equalTo(simulacao.getEmail()))
                .body("valor", equalTo(simulacao.getValor()))
                .body("parcelas", equalTo(simulacao.getParcelas()))
                .body("seguro", equalTo(simulacao.isSeguro()));
    }
    @Test
    public void testeInserirSimulacaoCpfComCaracteresEspeciaisSemSucesso() throws IOException {
        Simulacao simulacao = SimulacaoDataFactory.criarSimulacaoSemSucessoCpfComCaracteresEspeciais();

        given()
            .body(simulacao)
        .when()
            .post(SIMULACAO_CREDITO_ENDPOINT)
        .then()
            .assertThat()
                .statusCode(HttpStatus.SC_BAD_REQUEST)
                .body("id", nullValue());
    }
    @Test
    public void testeInserirSimulacaoCpfInvalido() throws IOException {
        Simulacao simulacao = SimulacaoDataFactory.criarSimulacaoComCpfInvalido();

        given()
            .body(simulacao)
        .when()
            .post(SIMULACAO_CREDITO_ENDPOINT)
        .then()
            .assertThat()
                .statusCode(HttpStatus.SC_BAD_REQUEST)
                .body("id", nullValue());
    }

    @Test
    public void testeInserirSimulacaoCpfDuplicado() throws IOException {
        Simulacao simulacao = SimulacaoDataFactory.criarSimulacaoComCpfDuplicado();

        given()
            .body(simulacao)
        .when()
            .post(SIMULACAO_CREDITO_ENDPOINT)
        .then()
            .assertThat()
                .statusCode(HttpStatus.SC_CONFLICT)
                .body("id", nullValue())
                .body("mensagem",equalToIgnoringCase("CPF duplicado"));
    }

    @Test
    public void testeInserirSimulacaoComEmailInvalido() throws IOException {
        Simulacao simulacao = SimulacaoDataFactory.criarSimulacaoComEmailInvalido();

        given()
            .body(simulacao)
        .when()
            .post(SIMULACAO_CREDITO_ENDPOINT)
        .then()
                .assertThat()
                    .statusCode(HttpStatus.SC_BAD_REQUEST)
                    .body("id", nullValue())
                    .body("erros.email", equalToIgnoringCase("não é um endereço de e-mail"));
    }
    @Test
    public void testeInserirSimulacaoComValorAcimaDoMaximo() throws IOException {
        Simulacao simulacao = SimulacaoDataFactory.criarSimulacaoComValorExcedente();

        given()
            .body(simulacao)
        .when()
            .post(SIMULACAO_CREDITO_ENDPOINT)
        .then()
            .assertThat()
                .statusCode(HttpStatus.SC_BAD_REQUEST)
                .body("id", nullValue())
                .body("erros.valor", equalToIgnoringCase("Valor deve ser menor ou igual a R$ 40.000"));
    }
    @Test
    public void testeInserirSimulacaoComValorAbaixoDoMinimo() throws IOException {
        Simulacao simulacao = SimulacaoDataFactory.criarSimulacaoComValorMenorQueMinimo();

        given()
            .body(simulacao)
        .when()
            .post(SIMULACAO_CREDITO_ENDPOINT)
        .then()
            .assertThat()
                .statusCode(HttpStatus.SC_BAD_REQUEST)
                .body("id", nullValue())
                .body("erros.valor", equalToIgnoringCase("Valor deve ser maior ou igual a R$ 1000"));
    }
    @Test
    public void testeInserirSimulacaoComValorNegativo() throws IOException {
        Simulacao simulacao = SimulacaoDataFactory.criarSimulacaoComValorNegativo();

        given()
            .body(simulacao)
        .when()
            .post(SIMULACAO_CREDITO_ENDPOINT)
        .then()
            .assertThat()
                .statusCode(HttpStatus.SC_BAD_REQUEST)
                .body("id", nullValue())
                .body("erros.valor", equalToIgnoringCase("Valor deve ser maior ou igual a R$ 1000 e menor ou igual a R$ 40.000"));
    }
    @Test
    public void testeInserirSimulacaoComValorDeCreditoMaximo() throws IOException {
        Simulacao simulacao = SimulacaoDataFactory.criarSimulacaoComValorDeCreditoMaximo();

        given()
            .body(simulacao)
        .when()
            .post(SIMULACAO_CREDITO_ENDPOINT)
        .then()
            .assertThat()
                .statusCode(HttpStatus.SC_CREATED)
                .body("id", notNullValue())
                .body("valor", equalTo(simulacao.getValor()));
    }

    @Test
    public void testeInserirSimulacaoComValorDeCreditoMinimo() throws IOException {
        Simulacao simulacao = SimulacaoDataFactory.criarSimulacaoComValorDeCreditoMinimo();

        given()
            .body(simulacao)
        .when()
            .post(SIMULACAO_CREDITO_ENDPOINT)
        .then()
            .assertThat()
                .statusCode(HttpStatus.SC_CREATED)
                .body("id", notNullValue())
                .body("valor", equalTo(simulacao.getValor()));
    }

    @Test
    public void testeInserirSimulacaoComParcelaMaximaPermitida() throws IOException {
        Simulacao simulacao = SimulacaoDataFactory.criarSimulacaoParcelaMaxima();

        given()
            .body(simulacao)
        .when()
            .post(SIMULACAO_CREDITO_ENDPOINT)
        .then()
            .assertThat()
                .statusCode(HttpStatus.SC_CREATED)
                .body("id", notNullValue())
                .body("parcelas", equalTo(simulacao.getParcelas()));
    }
    @Test
    public void testeInserirSimulacaoComParcelaAcimaDoMaximoPermitido() throws IOException {
        Simulacao simulacao = SimulacaoDataFactory.criarSimulacaoParcelaAcimaDoMaximo();

        given()
            .body(simulacao)
        .when()
            .post(SIMULACAO_CREDITO_ENDPOINT)
        .then()
            .assertThat()
               .statusCode(HttpStatus.SC_BAD_REQUEST)
                .body("id", nullValue())
                .body("parcelas", equalTo(simulacao.getParcelas()));
    }
    @Test
    public void testeInserirSimulacaoComParcelaMinimaPermitida() throws IOException {
        Simulacao simulacao = SimulacaoDataFactory.criarSimulacaoParcelaMinima();

        given()
            .body(simulacao)
        .when()
            .post(SIMULACAO_CREDITO_ENDPOINT)
        .then()
            .assertThat()
                .statusCode(HttpStatus.SC_CREATED)
                .body("id", notNullValue())
                .body("parcelas", equalTo(simulacao.getParcelas()));
    }
    @Test
    public void testeInserirSimulacaoComParcelaAbaixoDoMinimoPermitido() throws IOException {
        Simulacao simulacao = SimulacaoDataFactory.criarSimulacaoParcelaAbaixoDoMinimo();

        given()
            .body(simulacao).log().all()
        .when()
            .post(SIMULACAO_CREDITO_ENDPOINT)
        .then().log().all()
            .assertThat()
                .statusCode(HttpStatus.SC_BAD_REQUEST)
                .body("id", nullValue())
                .body("erros.parcelas", equalToIgnoringCase("Parcelas deve ser igual ou maior que 2"));
    }

}




