package br.com.sicredi.api.funcional;

import br.com.sicredi.api.factory.SimulacaoDataFactory;
import br.com.sicredi.api.pojo.Simulacao;
import br.com.sicredi.api.utils.GeradorDeDadosAleatorios;
import org.apache.http.HttpStatus;
import org.junit.Test;
import java.io.IOException;


import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.*;

public class TesteConsultarSimulacaoDeCredito extends TesteBase {
    private static final String SIMULACAO_CREDITO_ENDPOINT = "/simulacoes/";
    @Test
    public void testeConsultarSimulacaoComSucesso() throws IOException {
        Simulacao simulacao = SimulacaoDataFactory.criarSimulacaoComSucesso();
        String capturaCpf;

        capturaCpf = given()
            .body(simulacao)
        .when()
            .post(SIMULACAO_CREDITO_ENDPOINT)
        .then()
            .extract()
                .path("cpf");

        given()
        .when()
            .get(SIMULACAO_CREDITO_ENDPOINT + capturaCpf)
        .then()
            .assertThat()
                .statusCode(HttpStatus.SC_OK)
                .body("id", notNullValue())
                .body("nome", equalTo(simulacao.getNome()))
                .body("cpf", equalTo(simulacao.getCpf()))
                .body("email", equalTo(simulacao.getEmail()))
                .body("valor", equalTo(simulacao.getValor()))
                .body("parcelas", equalTo(simulacao.getParcelas()))
                .body("seguro", equalTo(simulacao.isSeguro()));
    }

    @Test
    public void testeConsultarSimulacaoInexistente() throws IOException {
        String cpfInexistenteNaBase = SimulacaoDataFactory.consultarSimulacaoInexistente();

        given()
        .when()
            .get(SIMULACAO_CREDITO_ENDPOINT+cpfInexistenteNaBase)
        .then()
            .assertThat()
                .statusCode(HttpStatus.SC_NOT_FOUND)
                .body("mensagem", equalToIgnoringCase("CPF " + cpfInexistenteNaBase + " não encontrado"));

    }

    @Test
    public void testeConsultarTodasSimulacoes() {
       int quantidadeSimulacoes;

       quantidadeSimulacoes = given()
        .when()
            .get(SIMULACAO_CREDITO_ENDPOINT)
        .then()
           .extract()
               .path("data.size()");

        given()
        .when()
            .get(SIMULACAO_CREDITO_ENDPOINT)
            .then()
                .assertThat()
                    .statusCode(HttpStatus.SC_OK)
                    .body("findAll {it.id}.size()", is(quantidadeSimulacoes));
    }
    @Test
    public void testeConsultarSimulacaoComRestricaoDeCredito() throws IOException {
        Simulacao simulacao = SimulacaoDataFactory.criarSimulacaoComCpfRestrito();
        String cpfRestrito;


        cpfRestrito = given()
            .body(simulacao)
        .when()
            .post(SIMULACAO_CREDITO_ENDPOINT)
        .then()
            .extract()
                .path("cpf");

        given()
        .when()
            .get(SIMULACAO_CREDITO_ENDPOINT+cpfRestrito)
            .then()
            .assertThat()
                .statusCode(HttpStatus.SC_NOT_FOUND)
                .body("mensagem", equalToIgnoringCase("CPF " + cpfRestrito + " possui restrição"));

    }

}
