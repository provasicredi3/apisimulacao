package br.com.sicredi.api.factory;


import br.com.sicredi.api.pojo.Simulacao;
import br.com.sicredi.api.utils.GeradorDeDadosAleatorios;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.FileInputStream;
import java.io.IOException;

public class SimulacaoDataFactory {
    public static Simulacao criarSimulacao() throws IOException {
        Simulacao simulacao = new Simulacao();

        String cpf = GeradorDeDadosAleatorios.gerarCpfValidoSemRestricao();
        String nome = GeradorDeDadosAleatorios.retornarNomeValido();
        String email = GeradorDeDadosAleatorios.retornarEmailValido();
        float valor = GeradorDeDadosAleatorios.retonarValorDeCreditoValido();
        int parcelas = GeradorDeDadosAleatorios.retornarNumeroDeParcelasValidas();
        boolean seguro = GeradorDeDadosAleatorios.retornarSeguroValido();

        simulacao.setCpf(cpf);
        simulacao.setNome(nome);
        simulacao.setEmail(email);
        simulacao.setValor(valor);
        simulacao.setParcelas(parcelas);
        simulacao.setSeguro(seguro);

        return simulacao;
    }

    public static Simulacao criarSimulacaoComSucesso() throws IOException {
        Simulacao simulacaoComSucesso = criarSimulacao();
        String cpf = GeradorDeDadosAleatorios.gerarCpfValidoSemRestricao();
        simulacaoComSucesso.setCpf(cpf);
        return simulacaoComSucesso;
    }

    public static Simulacao criarSimulacaoSemSucessoCpfComCaracteresEspeciais() throws IOException {
        Simulacao SemSucessoCpfComCaracteresEspeciais = criarSimulacao();
        String cpf = GeradorDeDadosAleatorios.gerarCpfValidoComCaracteresEspeciais();
        SemSucessoCpfComCaracteresEspeciais.setCpf(cpf);
        return SemSucessoCpfComCaracteresEspeciais;
    }

    public static Simulacao criarSimulacaoComCpfInvalido() throws IOException {
        Simulacao cpfInvalido = criarSimulacao();
        String cpf = GeradorDeDadosAleatorios.retornarCpfInvalido();
        cpfInvalido.setCpf(cpf);

        return cpfInvalido;
    }
    public static Simulacao criarSimulacaoComCpfRestrito() throws IOException {
        Simulacao cpfRestrito = criarSimulacao();
        String cpf = GeradorDeDadosAleatorios.retornarCpfRestrito();
        cpfRestrito.setCpf(cpf);

        return cpfRestrito;
    }
    public static Simulacao criarSimulacaoComCpfDuplicado() throws IOException {
        Simulacao cpfDuplicado = criarSimulacao();
        cpfDuplicado.setCpf("66414919004");

        return cpfDuplicado;
    }
    public static Simulacao criarSimulacaoComEmailInvalido() throws IOException {
        Simulacao emailInvalido = criarSimulacao();
        String email = GeradorDeDadosAleatorios.retornarEmailInvalido();
        emailInvalido.setEmail(email);

        return emailInvalido;
    }
    public static Simulacao criarSimulacaoComValorExcedente() throws IOException {
        Simulacao simulacaoComValorExcedente = criarSimulacao();

        float valor = GeradorDeDadosAleatorios.retornarValorDeCreditoMaiorQueValorMaximo();
        simulacaoComValorExcedente.setValor(valor);

        return simulacaoComValorExcedente;
    }
    public static Simulacao criarSimulacaoComValorMenorQueMinimo() throws IOException {
        Simulacao simulacaoComValorMenor = criarSimulacao();

        float valor = GeradorDeDadosAleatorios.retornarValorDeCreditoMenorQueValorMinimo();
        simulacaoComValorMenor.setValor(valor);

        return simulacaoComValorMenor;
    }
    public static Simulacao criarSimulacaoComValorNegativo() throws IOException {
        Simulacao simulacaoComValorNegativo = criarSimulacao();

        float valor = GeradorDeDadosAleatorios.retornarValorDeCreditoNegativo();
        simulacaoComValorNegativo.setValor(valor);

        return simulacaoComValorNegativo;
    }
    public static Simulacao criarSimulacaoComValorDeCreditoMaximo() throws IOException {
        Simulacao simulacaoComValorMaximo = criarSimulacao();

        float valor = GeradorDeDadosAleatorios.retornarValorDeCreditoMaximo();
        simulacaoComValorMaximo.setValor(valor);

        return simulacaoComValorMaximo;
    }
    public static Simulacao criarSimulacaoComValorDeCreditoMinimo() throws IOException {
        Simulacao simulacaoComValorMinimo = criarSimulacao();

        float valor = GeradorDeDadosAleatorios.retornarValorDeCreditoMinimo();
        simulacaoComValorMinimo.setValor(valor);

        return simulacaoComValorMinimo;
    }
    public static Simulacao criarSimulacaoParcelaMinima() throws IOException {
        Simulacao simulacaoParcelaMinimo = criarSimulacao();

        int parcela = GeradorDeDadosAleatorios.retornarNumeroDeParcelasMinima();
        simulacaoParcelaMinimo.setParcelas(parcela);

        return simulacaoParcelaMinimo;
    }
    public static Simulacao criarSimulacaoParcelaAbaixoDoMinimo() throws IOException {
        Simulacao simulacaoParcelaMinimo = criarSimulacao();

        int parcela = GeradorDeDadosAleatorios.retornarNumeroDeParcelasMenorQueParcelaMinima();
        simulacaoParcelaMinimo.setParcelas(parcela);

        return simulacaoParcelaMinimo;
    }
    public static Simulacao criarSimulacaoParcelaMaxima() throws IOException {
        Simulacao simulacaoParcelaMaxima = criarSimulacao();

        int parcela = GeradorDeDadosAleatorios.retornarNumeroDeParcelasMaxima();
        simulacaoParcelaMaxima.setParcelas(parcela);

        return simulacaoParcelaMaxima;
    }

    public static Simulacao criarSimulacaoParcelaAcimaDoMaximo() throws IOException {
        Simulacao simulacaoParcelaMaxima = criarSimulacao();

        int parcela = GeradorDeDadosAleatorios.retornarNumeroDeParcelasMaiorQueParcelaMaxima();
        simulacaoParcelaMaxima.setParcelas(parcela);

        return simulacaoParcelaMaxima;
    }

    public static String consultarSimulacaoInexistente() throws IOException {
        String cpfInvalido = GeradorDeDadosAleatorios.retornarCpfInvalido();
        return cpfInvalido ;
    }

}

