package br.com.sicredi.api.utils;

import com.github.javafaker.Faker;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;
import java.util.Random;

public class GeradorDeDadosAleatorios {

    private static final int VALOR_CREDITO_MAXIMO = 40000;
    private static final int VALOR_CREDITO_MINIMO = 1000;
    private static final int QUANTIDADE_DE_PARCELA_MAXIMA = 48;
    private static final int QUANTIDADE_DE_PARCELA_MINIMA = 2;
    private final static Faker FAKER = new Faker(new Locale("pt-br"));

    private final static List<String> CPF_COM_RESTRICAO = Arrays.asList(
            "97093236014",
            "60094146012",
            "84809766080",
            "62648716050",
            "26276298085",
            "01317496094",
            "55856777050",
            "19626829001",
            "24094592008",
            "58063164083"
    );
    public static String retornarCpfRestrito() {
        int aleatorio = new Random().nextInt(CPF_COM_RESTRICAO.size());
        return CPF_COM_RESTRICAO.get(aleatorio);
    }

    public static String retornarCpfInvalido(){
        return FAKER.numerify("###########");

    }
    private static String gerarCaracteresEspeciaisCpf(String cpf) {
        return new StringBuilder(cpf).insert(3, ".").insert(7, ".").insert(11, "-").toString();
    }
    public static String gerarCpfValidoComCaracteresEspeciais() {
        String cpf = gerarCpfValidoSemRestricao();

        return gerarCaracteresEspeciaisCpf(cpf);
    }

    public static String gerarCpfValidoSemRestricao() {
        int d1;
        int d2;
        int divisao;
        Random random = new Random();

        int n1 = random.nextInt(10);
        int n2 = random.nextInt(10);
        int n3 = random.nextInt(10);
        int n4 = random.nextInt(10);
        int n5 = random.nextInt(10);
        int n6 = random.nextInt(10);
        int n7 = random.nextInt(10);
        int n8 = random.nextInt(10);
        int n9 = random.nextInt(10);
        int soma = n9 * 2 + n8 * 3 + n7 * 4 + n6 * 5 + n5 * 6 + n4 * 7 + n3 * 8 + n2 * 9 + n1 * 10;
        int valor = (soma / 11) * 11;

        d1 = soma - valor;
        divisao = d1 % 11;
        d1 = d1 < 2 ? 0 : 11 - divisao;

        int soma2 = d1 * 2 + n9 * 3 + n8 * 4 + n7 * 5 + n6 * 6 + n5 * 7 + n4 * 8 + n3 * 9 + n2 * 10 + n1 * 11;
        int valor2 = (soma2 / 11) * 11;

        d2 = soma2 - valor2;
        divisao = d2 % 11;
        d2 = d1 < 2 ? 0 : 11 - divisao;

        String textoNumeroConcatenado = String.valueOf(n1) + n2 + n3 + n4 + n5 + n6 + n7 + n8 + n9;
        String resultadoDigitos = String.valueOf(d1) + d2;
        return textoNumeroConcatenado + resultadoDigitos;
    }
    public static String retornarNomeValido(){
        return FAKER.name().fullName();

    }
    public static String retornarEmailValido(){
        return FAKER.internet().emailAddress();
    }
    public static String retornarEmailInvalido(){
        return FAKER.name().lastName()+"email.com";
    }

    public static float retonarValorDeCreditoValido(){
        return (float) FAKER.number().randomDouble(2, VALOR_CREDITO_MINIMO, VALOR_CREDITO_MAXIMO);
    }
    public static float retornarValorDeCreditoMaiorQueValorMaximo(){
        return (float) (VALOR_CREDITO_MAXIMO + 0.1);
    }
    public static float retornarValorDeCreditoMenorQueValorMinimo(){
        return (float) (VALOR_CREDITO_MINIMO - 0.1);
    }
    public static float retornarValorDeCreditoNegativo(){
        return (float) (VALOR_CREDITO_MINIMO - VALOR_CREDITO_MINIMO - (0.1));
    }
    public static float retornarValorDeCreditoMaximo(){
        return (float) VALOR_CREDITO_MAXIMO;
    }
    public static float retornarValorDeCreditoMinimo(){
        return (float) VALOR_CREDITO_MINIMO;
    }

    public static int retornarNumeroDeParcelasValidas(){
        return FAKER.number().numberBetween(QUANTIDADE_DE_PARCELA_MINIMA, QUANTIDADE_DE_PARCELA_MAXIMA);
    }

    public static int retornarNumeroDeParcelasMenorQueParcelaMinima(){
        int a = QUANTIDADE_DE_PARCELA_MINIMA -1;
        return a;
    }

    public static int retornarNumeroDeParcelasMaiorQueParcelaMaxima(){
        return QUANTIDADE_DE_PARCELA_MAXIMA +1;
    }
    public static int retornarNumeroDeParcelasMinima(){
        return QUANTIDADE_DE_PARCELA_MINIMA;
    }
    public static int retornarNumeroDeParcelasMaxima(){
        return QUANTIDADE_DE_PARCELA_MAXIMA;
    }
    public static boolean retornarSeguroValido(){
        return FAKER.bool().bool();
    }

}
