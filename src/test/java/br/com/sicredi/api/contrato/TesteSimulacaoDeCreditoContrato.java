package br.com.sicredi.api.contrato;

import br.com.sicredi.api.factory.SimulacaoDataFactory;
import br.com.sicredi.api.funcional.TesteBase;
import br.com.sicredi.api.pojo.Simulacao;
import org.apache.http.HttpStatus;
import org.junit.Test;
import java.io.IOException;
import static io.restassured.RestAssured.*;
import static io.restassured.module.jsv.JsonSchemaValidator.matchesJsonSchemaInClasspath;
import static org.hamcrest.Matchers.*;
import static org.hamcrest.Matchers.equalToIgnoringCase;


public class TesteSimulacaoDeCreditoContrato extends TesteBase {

    private static final String SIMULACAO_CREDITO_ENDPOINT = "/simulacoes/";
    @Test
    public void testeInserirSimulacaoContrato() throws IOException {
        Simulacao simulacao = SimulacaoDataFactory.criarSimulacaoComSucesso();

        given()
            .body(simulacao)
        .when()
            .post(SIMULACAO_CREDITO_ENDPOINT)
        .then()
            .assertThat()
                .body(matchesJsonSchemaInClasspath("schemas/postSimulacaoComSucessoContrato.json"));
    }

    @Test
    public void testeConsultarTodasSimulacoesContrato() throws IOException {

        given()
        .when()
            .get(SIMULACAO_CREDITO_ENDPOINT)
        .then()
            .assertThat()
                .body(matchesJsonSchemaInClasspath("schemas/getTodasSimulacoes.json"));
    }

    @Test
    public void testeInserirSimulacaoCpfDuplicadoContrato() throws IOException {
        Simulacao simulacao = SimulacaoDataFactory.criarSimulacaoComCpfDuplicado();

        given()
            .body(simulacao)
        .when()
            .post(SIMULACAO_CREDITO_ENDPOINT)
        .then()
            .assertThat()
                .body(matchesJsonSchemaInClasspath("schemas/getTodasSimulacoes.json"));

    }

    @Test
    public void testeConsultarSimulacaoComSucessoContrato() throws IOException {
        Simulacao simulacao = SimulacaoDataFactory.criarSimulacaoComSucesso();
        String capturaCpf;

        capturaCpf = given()
            .body(simulacao)
        .when()
            .post(SIMULACAO_CREDITO_ENDPOINT)
        .then()
            .extract()
                .path("cpf");

        given()
        .when()
            .get(SIMULACAO_CREDITO_ENDPOINT + capturaCpf)
        .then()
            .assertThat()
                .body(matchesJsonSchemaInClasspath("schemas/getSimulacaoPorCpf.json"));
    }

    @Test
    public void testeConsultarSimulacaoComRestricaoDeCredito() throws IOException {
        Simulacao simulacao = SimulacaoDataFactory.criarSimulacaoComCpfRestrito();
        String cpfRestrito;


        cpfRestrito = given()
            .body(simulacao)
        .when()
            .post(SIMULACAO_CREDITO_ENDPOINT)
        .then()
            .extract()
                .path("cpf");

        given()
        .when()
            .get(SIMULACAO_CREDITO_ENDPOINT+cpfRestrito)
        .then()
            .assertThat()
                .body(matchesJsonSchemaInClasspath("schemas/getSimulacaoCpfRestrito.json"));

    }

    @Test
    public void testeAlterarSimulacaoComSucesso() throws IOException {
        Simulacao simulacao = SimulacaoDataFactory.criarSimulacaoComSucesso();
        Simulacao simulacaoAlterada = SimulacaoDataFactory.criarSimulacaoComSucesso();
        String capturaCpf;

        capturaCpf = given()
            .body(simulacao)
        .when()
            .post(SIMULACAO_CREDITO_ENDPOINT)
        .then()
            .extract()
                .path("cpf");

        given()
            .body(simulacaoAlterada)
        .when()
            .put(SIMULACAO_CREDITO_ENDPOINT+capturaCpf)
        .then()
            .assertThat()
                .body(matchesJsonSchemaInClasspath("schemas/putSimulacaoComSucesso.json"));
    }

    @Test
    public void testeAlterarSimulacaoCpfNaoEncontradoContrato() throws IOException {
        Simulacao simulacao = SimulacaoDataFactory.criarSimulacaoComSucesso();
        String cpf = simulacao.getCpf();

        given()
            .body(simulacao)
        .when()
            .put(SIMULACAO_CREDITO_ENDPOINT+cpf)
        .then()
            .assertThat()
                .body(matchesJsonSchemaInClasspath("schemas/putSimulacaoSemSucesso.json"));
    }


}

