package br.com.sicredi.api.contrato;

import br.com.sicredi.api.funcional.TesteBase;
import br.com.sicredi.api.utils.GeradorDeDadosAleatorios;
import org.apache.http.HttpStatus;
import org.junit.Test;
import static io.restassured.RestAssured.given;
import static io.restassured.module.jsv.JsonSchemaValidator.matchesJsonSchemaInClasspath;

public class TesteRestricaoDeCreditoContrato extends TesteBase {
    private static final String CONSULTA_CPF_RESTRITO_ENDPOINT = "/restricoes/";
    @Test
    public void testeConsultarRestricaoContrato(){
        String cpf = GeradorDeDadosAleatorios.retornarCpfRestrito();

        given()
        .when()
            .get(CONSULTA_CPF_RESTRITO_ENDPOINT+ cpf)
        .then()
            .assertThat()
                .statusCode(HttpStatus.SC_OK)
                .body(matchesJsonSchemaInClasspath("schemas/getCpfComRestricao.json"));
    }
}
