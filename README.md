# Sicredi - Desafio Automação API

**OBSERVAÇÕES IMPORTANTES SOBRE O DESAFIO:**
* O projeto foi automatizado separadamento da API pois as versões que eu teriam que utilizar do Rest-assured e outros teria que ser muito antigas.
* Os testes do método PUT da simulação de crédito foram implementados somente para as regras que eram diferentes do método POST, pois a API utiliza o mesmo objeto para ambos. Dessa forma, somente seria necessário uma automação caso precise mudar essa lógica em uma versão futura da API.
* Os cenários de testes com status de execução estão na raiz do projeto em formato word. 

## Requisitos

* JDK 8 ou superior
*  <a href="https://maven.apache.org/download.cgi" target="_blank">Apache Maven</a>

**Observação:** Adicionar o Maven e o JDK como uma variável de ambiente no seu sistema operacional.

## Ferramentas e bibliotecas
* <a href="https://junit.org/junit4/" target="_blank">JUnit4</a>
*  <a href="https://rest-assured.io/" target="_blank">Rest Assured</a>
* <a href="https://github.com/DiUS/java-faker" target="_blank">Java Faker</a>
* <a href="https://qameta.io/allure-report/" target="_blank">Allure reports</a>
* <a href="https://owner.aeonbits.org/" target="_blank">Owner</a>

## Comandos para execução dos testes

Na raiz do projeto execute:

* Testes
```bash
  mvn test
 ```

* Relatório consolidado
```bash
  allure serve "relativePath da pasta /target/surefire-reports"
 ```

--------------------------------------------
## Cenários de Testes com falha

```
Cenário: Enviar uma requisição de simulação de crédito com CPF contendo caracteres especiais
```
* Motivo da falha: não há regra implementada para validar se o cpf está no formato 99.999.999-99, além do campo aceitar valores como “1”, “20”, dentre outros.
 
* Sugestão de melhoria: implementar regra de doc().

```
Cenário: Enviar uma requisição de simulação de crédito com CPF inválido
```
* Motivo da falha: não há regra implementada para validar se o cpf está no formato 99.999.999-99, além do campo aceitar valores como “1”, “20”, dentre outros.
* Sugestão de melhoria: implementar regra de limite de caracteres min(11), max(11), doc().
```
Cenário: Enviar uma requisição de simulação de crédito com CPF duplicado
```

* Motivo da falha: a regra está implementada, porém o status code retornado é 400 e não 409 conforme documento de requisitos.

```
Cenário: Enviar uma requisição de simulação de crédito com valor abaixo do permitido
```
* Motivo da falha: não há regra implementada para validar se o valor é <= 1000.

```
Cenário: Enviar uma requisição de simulação de crédito com valor negativo
```
* Motivo da falha: não há regra implementada para validar se o valor é <= 1000.

```
Cenário: Enviar uma requisição de simulação de crédito com número de parcelas acima do máximo permitido
```
* Motivo da falha: não há regra implementada para validar se o número de parcelas é > 48.

```
Cenário: Alterar uma simulação de crédito com sucesso
```
* Motivo da falha: a regra está implementada, porém o status code retornado é 200 e não 201 conforme documento de requisitos.

```
Cenário: Alterar uma simulação de crédito com sucesso
```
* Motivo da falha: a regra está implementada, porém o status code retornado é 200 e não 201 conforme documento de requisitos.

```
Cenário: Consultar todas as simulações de crédito sem sucesso
```
* Motivo da falha: a regra está implementada, porém o status code retornado é 200 e não 204 conforme documento de requisitos.

```
Cenário: Deletar uma simulação de crédito inexistente
```
* Motivo da falha: não há regra implementada para validar se existe ou não um registro, de acordo com seu retorno que é sempre 200.
* Sugestão de melhoria: implementar regra de validação quando não existe o id pesquisado para deletar.

```
Cenário: Enviar uma requisição sem informar o tipo de aplicação no cabeçalho
```
* Sugestão de melhoria: implementar o content-type do tipo JSON, pois a API é neste formato. Atualmente a é possível enviar requisições sem o content-type preenchido.


## Relatório com Overview dos Testes


![img.png](img.png)
